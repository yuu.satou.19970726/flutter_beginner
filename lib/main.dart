import 'package:flutter/material.dart';

void main() =>
    runApp(MaterialApp(debugShowCheckedModeBanner: false, home: Home()));

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('flutter beginner'),
        centerTitle: true,
        backgroundColor: Colors.black,
      ),
      body: const Center(
        // child: Text(
        //   'hello Holmes!',
        //   style: TextStyle(
        //       fontSize: 20.0,
        //       fontWeight: FontWeight.bold,
        //       letterSpacing: 2.0,
        //       color: Colors.black,
        //       fontFamily: 'IndieFlowerRegular'),
        // ),
        // child: Image(
        //     image: NetworkImage(
        //         'https://maytinhvui.com/wp-content/uploads/2021/02/Hinh-nen-anime-12-min.png')),
        child: Image(image: AssetImage('assets/your_name.png')),
      ),
      floatingActionButton: const FloatingActionButton(
        onPressed: null,
        child: Text('click'),
        backgroundColor: Colors.black,
      ),
    );
  }
}
